package com.hendisantika.retrofitsample.utils

import android.view.View

/**
 * Created by hendisantika on 14/10/18  08.22.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
fun View.visible() {
    visibility = View.VISIBLE
}

fun View.inVisible() {
    visibility = View.INVISIBLE
}