package com.hendisantika.retrofitsample.api

import android.net.Uri
import com.hendisantika.retrofitsample.BuildConfig

/**
 * Created by hendisantika on 14/10/18  06.19.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
object TheSportDBApi {
//    fun getTeams(league: String?): String {
//        return BuildConfig.BASE_URL + "api/v1/json/${BuildConfig.TSDB_API_KEY}" + "/search_all_teams.php?l=" + league
//    }


    fun getTeams(league: String?): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
            .appendPath("api")
            .appendPath("v1")
            .appendPath("json")
            .appendPath(BuildConfig.TSDB_API_KEY)
            .appendPath("search_all_teams.php")
            .appendQueryParameter("l", league)
            .build()
            .toString()
    }
}