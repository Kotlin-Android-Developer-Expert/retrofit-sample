package com.hendisantika.retrofitsample.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by hendisantika on 14/10/18  06.26.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Team(
    @SerializedName("idTeam")
    var teamId: String? = null,

    @SerializedName("strTeam")
    var teamName: String? = null,

    @SerializedName("strTeamBadge")
    var teamBadge: String? = null
)