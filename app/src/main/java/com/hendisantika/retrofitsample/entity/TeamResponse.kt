package com.hendisantika.retrofitsample.entity

/**
 * Created by hendisantika on 14/10/18  06.29.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class TeamResponse(
    val teams: List<Team>
)