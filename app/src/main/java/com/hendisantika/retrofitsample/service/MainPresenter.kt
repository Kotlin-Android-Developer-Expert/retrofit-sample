package com.hendisantika.retrofitsample.service

import com.google.gson.Gson
import com.hendisantika.retrofitsample.api.TheSportDBApi
import com.hendisantika.retrofitsample.entity.TeamResponse
import com.hendisantika.retrofitsample.repository.ApiRepository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by hendisantika on 14/10/18  06.31.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MainPresenter(
    private val view: MainView,
    private val apiRepository: ApiRepository,
    private val gson: Gson
) {
    fun getTeamList(league: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository
                    .doRequest(TheSportDBApi.getTeams(league)),
                TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamList(data.teams)
            }

        }
    }
}