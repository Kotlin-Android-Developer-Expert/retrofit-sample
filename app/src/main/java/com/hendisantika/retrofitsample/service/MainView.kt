package com.hendisantika.retrofitsample.service

import com.hendisantika.retrofitsample.entity.Team

/**
 * Created by hendisantika on 14/10/18  06.31.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}