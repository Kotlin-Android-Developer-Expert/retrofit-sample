package com.hendisantika.retrofitsample.repository

import java.net.URL

/**
 * Created by hendisantika on 14/10/18  06.18.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class ApiRepository {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}